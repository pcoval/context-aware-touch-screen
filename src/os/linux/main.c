// -*- mode: C++; c-basic-offset: 4; indent-tabs-mode: nil -*-
// vim: ft=cpp:expandtab:ts=4:sw=4:softtabstop=4:
// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-License-Identifier: MIT

#define _DEFAULT_SOURCE /* needed for usleep() */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <lvgl/lvgl.h>

static void hal_init(void); /* see {sdl,...}.c */

#include "widgets/msgbox.h"
#include "widgets/yesno.h"
#include "widgets/inputbox.h"
#include "widgets/checklist.h"

#include <coap3/coap.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

char * usage(int argc, char ** argv)
{
    printf("Usage: %s, <COMMAND> [OPTIONS]\n\
\n\
Description:\n\
 Basic UI utility to be used in scripts.\n\
 Inspired by ncurses' dialog, implemented using LVGL\n\
\n\
Commands: \n\
 --msgbox \"message\" 0 0\n\
 --yesno  \"message\" 0 0\n\
 --inputbox \"message\" 0 0\n\
 --checklist  \"message\" 0 0 0 \"keyword1\" \"Description1\" \"ON\" ... \n\
\n\
", argv[0]);
}


void default_ui_init(int argc, char ** argv)
{
    usage(argc, argv);
    char * msgbox_argv[] = {
        argv[0],
        "--msgbox",
        "Basic UI utility to be used in scripts.\n\
Inspired by ncurses dialog, implemented using LVGL\n\
For help run again with '--help' command option"
    };
    msgbox_ui_init(3, msgbox_argv);
}


void ui_init(int argc, char ** argv)
{
    lv_obj_set_flex_flow(lv_scr_act(), LV_FLEX_FLOW_COLUMN);
    lv_obj_set_flex_align(lv_scr_act(),
                          LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER,
                          LV_FLEX_ALIGN_CENTER);
    if((argc >= 1) && (argv[1])) {
        if(strcmp(argv[1], "--msgbox") == 0) {
            msgbox_ui_init(argc, argv);
        }
        else if(strcmp(argv[1], "--yesno") == 0) {
            yesno_ui_init(argc, argv);
        }
        else if(strcmp(argv[1], "--inputbox") == 0) {
            inputbox_ui_init(argc, argv);
        }
        else if(strcmp(argv[1], "--checklist") == 0) {
            checklist_ui_init(argc, argv);
        }
        else {
            default_ui_init(argc, argv);
        }
    }
    else {
        default_ui_init(argc, argv);
    }
}

/*
 * ============================================================================
 * =============================== CATS =======================================
 * ============================================================================
 */

LV_IMG_DECLARE(logo_img);
LV_IMG_DECLARE(key_0);
LV_IMG_DECLARE(key_1);
LV_IMG_DECLARE(key_2);
LV_IMG_DECLARE(key_3);
LV_IMG_DECLARE(key_4);
LV_IMG_DECLARE(key_5);
LV_IMG_DECLARE(key_6);
LV_IMG_DECLARE(key_7);
LV_IMG_DECLARE(key_8);
LV_IMG_DECLARE(key_9);
LV_IMG_DECLARE(key_asterisk);
LV_IMG_DECLARE(key_hash);

#define NUM_ROWS 4
#define NUM_COLS 3

static const lv_coord_t col_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_coord_t row_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_img_dsc_t *key_imgs[NUM_ROWS][NUM_COLS] = {
	{ &key_7, &key_8, &key_9 },
	{ &key_4, &key_5, &key_6 },
	{ &key_1, &key_2, &key_3 },
	{ &key_asterisk, &key_0, &key_hash },
};

static char *data[NUM_ROWS][NUM_COLS] = {
	{ "7", "8", "9" },
	{ "4", "5", "6" },
	{ "1", "2", "3" },
	{ "*", "0", "#" },
};

static lv_style_t pressed_style;

int
resolve_address(const char *host, const char *service, coap_address_t *dst) {

	struct addrinfo *res, *ainfo;
	struct addrinfo hints;
	int error, len=-1;

	memset(&hints, 0, sizeof(hints));
	memset(dst, 0, sizeof(*dst));
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_family = AF_UNSPEC;

	error = getaddrinfo(host, service, &hints, &res);

	if (error != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
		return error;
	}

	for (ainfo = res; ainfo != NULL; ainfo = ainfo->ai_next) {
		switch (ainfo->ai_family) {
			case AF_INET6:
			case AF_INET:
				len = dst->size = ainfo->ai_addrlen;
				memcpy(&dst->addr.sin6, ainfo->ai_addr, dst->size);
				goto finish;
			default:
				;
		}
	}

	finish:
	freeaddrinfo(res);
	return len;
}

static void on_button_pressed(lv_event_t *event)
{
	char *key = (char *)lv_event_get_user_data(event);
	fprintf(stderr, "%s button pressed\n", key);

	coap_context_t  *ctx = NULL;
	coap_session_t *session = NULL;
	coap_address_t dst;
	coap_pdu_t *pdu = NULL;
	int result = EXIT_FAILURE;;
	const uint8_t *payload = key;
	int ret = 0;

	coap_startup();

	/* Set logging level */
	coap_set_log_level(LOG_WARNING);

	/* resolve destination address where server should be sent */
	if (resolve_address("2001::1:1", "5683", &dst) < 0) {
		coap_log(LOG_CRIT, "failed to resolve address\n");
		goto finish;
	}

	/* create CoAP context and a client session */
	if (!(ctx = coap_new_context(NULL))) {
		coap_log(LOG_EMERG, "cannot create libcoap context\n");
		goto finish;
	}
	/* Support large responses */
	coap_context_set_block_mode(ctx,
					COAP_BLOCK_USE_LIBCOAP | COAP_BLOCK_SINGLE_BODY);
	if (!(session = coap_new_client_session(ctx, NULL, &dst,
													COAP_PROTO_UDP))) {
		coap_log(LOG_EMERG, "cannot create client session\n");
		goto finish;
	}

	/* construct CoAP message */
	pdu = coap_pdu_init(COAP_MESSAGE_NON,
						COAP_REQUEST_CODE_PUT,
						coap_new_message_id(session),
						coap_session_max_pdu_size(session));
	if (!pdu) {
		coap_log( LOG_EMERG, "cannot create PDU\n" );
		goto finish;
	}

	coap_add_data(pdu, 1, payload);

	coap_show_pdu(LOG_WARNING, pdu);

	/* and send the PDU */
	ret = coap_send(session, pdu);

	if(ret == 0) {
		printf("Error while sending packet\n");
	}

	result = EXIT_SUCCESS;
	finish:

	coap_session_release(session);
	coap_free_context(ctx);
	coap_cleanup();

	return;
}

int main(int argc, char ** argv)
{
	lv_obj_t *scr, *keypad, *btn, *logo;
	int i, j;

	lv_init();
	hal_init();

	lv_style_init(&pressed_style);
	lv_style_set_img_recolor_opa(&pressed_style, LV_OPA_30);
	lv_style_set_img_recolor(&pressed_style, lv_color_white());

	lv_theme_default_init(NULL,
			      lv_palette_main(LV_PALETTE_BLUE),
			      lv_palette_main(LV_PALETTE_RED),
			      true,
			      &lv_font_montserrat_14);

	scr = lv_obj_create(lv_scr_act());
	lv_obj_set_size(scr, 600, 1024);
	lv_obj_align(scr, LV_ALIGN_CENTER, 0, 0);

	logo = lv_img_create(scr);
	lv_img_set_src(logo, &logo_img);
	lv_obj_align(logo, LV_ALIGN_TOP_MID, 0, 0);

	keypad = lv_obj_create(scr);
	lv_obj_set_style_grid_column_dsc_array(keypad, col_dsc, 0);
	lv_obj_set_style_grid_row_dsc_array(keypad, row_dsc, 0);
	lv_obj_set_size(keypad, 600, 800);
	lv_obj_set_layout(keypad, LV_LAYOUT_GRID);
	lv_obj_align(keypad, LV_ALIGN_BOTTOM_MID, 0, 0);

	for (i = 0; i < NUM_ROWS; i++) {
		for (j = 0; j < NUM_COLS; j++) {
			btn = lv_imgbtn_create(keypad);
			lv_imgbtn_set_src(btn, LV_IMGBTN_STATE_RELEASED,
					  NULL, key_imgs[i][j], NULL);
			lv_obj_add_style(btn, &pressed_style, LV_STATE_PRESSED);
			lv_obj_add_event_cb(btn, on_button_pressed,
					    LV_EVENT_CLICKED, (void *) data[i][j]);
			lv_obj_set_size(btn, 180, 180);
			lv_obj_set_grid_cell(btn,
					     LV_GRID_ALIGN_SPACE_EVENLY, j, 1,
					     LV_GRID_ALIGN_SPACE_EVENLY, i, 1);
		}
	}

	for (;;) {
		lv_timer_handler();
		usleep(5 * 1000);
	}

	return 0;
}
